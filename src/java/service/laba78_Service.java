/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import db.Article;
import db.Category;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author user
 */
@WebService(serviceName = "laba78_Service")
@Stateless()
public class laba78_Service {

    /**
     * This is a sample web service operation
     */
    @PersistenceContext
    private EntityManager entityManager;

    @WebMethod(operationName = "getRootCategories")
    public List<Object[]> getRootCategories() {
        Query query = entityManager.createNamedQuery("Category.findIsNulls");
        List<Category> categories = query.getResultList();
        ArrayList<Object[]> categoriesArrayList = new ArrayList<Object[]>();

        for (int i = 0; i < categories.size(); i++) {
            Integer id = categories.get(i).getId();
            String name = categories.get(i).getName();
            categoriesArrayList.add(new Object[]{id, name, null});
        }
        return categoriesArrayList;
    }

    @WebMethod(operationName = "getCategories")
    public List<Object[]> getCategories(@WebParam(name = "categoryId") Integer categoryId) {
        Query query = entityManager.createNamedQuery("Category.findByCategoryId");
        query.setParameter("categoryId", cfindById(categoryId));
        List<Category> categories = query.getResultList();
        ArrayList<Object[]> categoriesArrayList = new ArrayList<Object[]>();

        for (Category category : categories) {
            categoriesArrayList.add(new Object[]{category.getId(), category.getName(), category.getCategoryId().getId()});
        }
        return categoriesArrayList;
    }

    @WebMethod(operationName = "getCategory")
    public Object[] getCategory(@WebParam(name = "id") Integer id) {
        Category category = (Category) cfindById(id);
        Category tmpCategory = category.getCategoryId();
        return new Object[]{category.getId(), category.getName(), tmpCategory == null ? null : tmpCategory.getId()};
    }

    @WebMethod(operationName = "getArticle")
    public Object[] getArticle(@WebParam(name = "id") Integer id) {
        Article article = (Article) afindById(id);
        return new Object[]{article.getId(), article.getTitle(), article.getContent(), article.getCategoryId().getId()};
    }

    @WebMethod(operationName = "getArticles")
    public List<Object[]> getArticles(@WebParam(name = "categoryId") Integer categoryId) {

        Query query = entityManager.createNamedQuery("Article.findByCategoryId");
        query.setParameter("categoryId", cfindById(categoryId));
        List<Article> articles = query.getResultList();
        ArrayList<Object[]> articlesArrayList = new ArrayList<Object[]>();
        for (Article article : articles) {
            articlesArrayList.add(new Object[]{article.getId(), article.getTitle(), article.getContent(), article.getCategoryId().getId()});
        }
        return articlesArrayList;
    }

    @WebMethod(operationName = "deleteCategory")
    public void deleteCategory(@WebParam(name = "id") Integer id) {
        entityManager.createNativeQuery(String.format("delete from category where id=%d", id)).executeUpdate();
    }

    @WebMethod(operationName = "deleteArticle")
    public void deleteArticle(@WebParam(name = "id") Integer id) {
        entityManager.createNativeQuery(String.format("delete from article where id=%d", id)).executeUpdate();
    }

    @WebMethod(operationName = "deleteArticles")
    public void deleteArticles(@WebParam(name = "categoryId") Integer categoryId) {
        entityManager.createNativeQuery(String.format("delete from article where category_id=%d", categoryId)).executeUpdate();
    }

    @WebMethod(operationName = "updateArticle")
    public void updateArticle(@WebParam(name = "id") Integer id, @WebParam(name = "title") String title, @WebParam(name = "content") String content, @WebParam(name = "categoryId") Integer categoryId) {
        Article article = afindById(id);
        article.setCategoryId(cfindById(categoryId));
        article.setTitle(title);
        article.setContent(content);

    }

    @WebMethod(operationName = "updateCategory")
    public void updateCategory(@WebParam(name = "id") Integer id, @WebParam(name = "name") String name, @WebParam(name = "categoryId") Integer categoryId) {
        Category category;
        System.out.println(categoryId);
        if (categoryId == null) {
            category = (Category) entityManager.createNamedQuery("Category.findIsNulls").getSingleResult();
            category.setCategoryId(null);

        } else {
            category = cfindById(id);
            category.setCategoryId(cfindById(categoryId));
        }
        category.setName(name);


    }

    @WebMethod(operationName = "addCategory")
    public void addCategory(@WebParam(name = "name") String name, @WebParam(name = "categoryId") Integer categoryId) {
        entityManager.createNativeQuery(String.format("insert into category (`name`, `category_id`) values ('%s',%d)", name, categoryId)).executeUpdate();

    }

    @WebMethod(operationName = "addArticle")
    public void addArticle(@WebParam(name = "title") String title, @WebParam(name = "content") String content, @WebParam(name = "categoryId") Integer categoryId) {
        entityManager.createNativeQuery(String.format("insert into article (`title`, `content` ,`category_id`) values ('%s','%s',%d)", title, content, categoryId)).executeUpdate();
    }

    private Article afindById(Integer id) {
        Query query = entityManager.createNamedQuery("Article.findById");
        query.setParameter("id", id);
        Article article = (Article) query.getSingleResult();
        return article;
    }

    private Category cfindById(Integer id) {
        Query query = entityManager.createNamedQuery("Category.findById");
        query.setParameter("id", id);
        Category category = (Category) query.getSingleResult();
        return category;
    }
}
